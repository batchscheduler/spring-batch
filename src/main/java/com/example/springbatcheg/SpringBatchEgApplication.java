package com.example.springbatcheg;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing
public class SpringBatchEgApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBatchEgApplication.class, args);
	}

}
