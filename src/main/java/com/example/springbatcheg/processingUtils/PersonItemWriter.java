package com.example.springbatcheg.processingUtils;

import com.example.springbatcheg.model.Person;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class PersonItemWriter implements ItemWriter<Person> {
    @Override
    public void write(List<? extends Person> list) throws Exception {
        for (Person person :
                list) {
            System.out.println("Person  Item Writer" + person);
        }
    }
}
