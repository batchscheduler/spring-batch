package com.example.springbatcheg.processingUtils;

import com.example.springbatcheg.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class PersonItemProcessor implements ItemProcessor<Person,Person> {

    private  static Logger log = LoggerFactory.getLogger(PersonItemProcessor.class);
    @Override
    public Person process(Person person) throws Exception {
        String firstName=person.getFirstName().toUpperCase();
        String lastName =person.getLastName().toUpperCase();
        Person person1 = new Person(firstName,lastName);
        log.info("Person after item processor is "+person1);
        return person;
    }
}
