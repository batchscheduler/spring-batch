package com.example.springbatcheg.configurations;

import com.example.springbatcheg.model.Person;
import com.example.springbatcheg.processingUtils.PersonItemProcessor;
import com.example.springbatcheg.processingUtils.PersonItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Value("${input}")
    Resource resource;

    // create ItemReader

    @Bean
    public FlatFileItemReader<Person> reader() {
        return new FlatFileItemReaderBuilder<Person>()
                .name("personItemReader")
                .resource(resource)
                .delimited()
                .names(new String[]{"firstName", "lastName"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                    setTargetType(Person.class);
                }})
                .build();
    }

    // create ItemProcessor
    @Bean
    public PersonItemProcessor processor() {
        return new PersonItemProcessor();
    }

    // create ItemWriter
    @Bean
    public PersonItemWriter writer() {
        return new PersonItemWriter();
    }
    // create Steps for jobs
    @Bean
    public Step step1(){
        return stepBuilderFactory
                .get("step1")
                .<Person,Person>chunk(5)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();

    }
    // create job
    @Bean
    public Job job(){
        return jobBuilderFactory
                .get("job")
                .incrementer(new RunIdIncrementer())
                .start(step1())
                .build();
    }
}
